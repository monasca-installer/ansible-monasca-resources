# monasca-resources

## Update License Information

- When you add new OSS, update NOTICE.txt
- Add license file when there is no file of license used by added OSS

## Note

Clone/push may be failed because the remote end hung up unexpectedly.
Then, following command may help.

```
git config --global http.postBuffer 524288000
```

## License

Apache License, Version 2.0

## Author Information

FUJITSU LIMITED
